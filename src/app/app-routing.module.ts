import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthorProfileComponent } from './components/pages/author-profile/author-profile.component';
import { ComingSoonComponent } from './components/pages/coming-soon/coming-soon.component';
import { DashboardAddListingsComponent } from './components/pages/dashboard/dashboard-add-listings/dashboard-add-listings.component';
import { DashboardBookingsComponent } from './components/pages/dashboard/dashboard-bookings/dashboard-bookings.component';
import { DashboardMyListingsComponent } from './components/pages/dashboard/dashboard-my-listings/dashboard-my-listings.component';
import { DashboardMyProfileComponent } from './components/pages/dashboard/dashboard-my-profile/dashboard-my-profile.component';
import { DashboardComponent } from './components/pages/dashboard/dashboard.component';
import { GridListingsFullWidthComponent } from './components/pages/grid-listings-full-width/grid-listings-full-width.component';
import { HomeDemoOneComponent } from './components/pages/home-demo-one/home-demo-one.component';
import { ListingsDetailsComponent } from './components/pages/listings-details/listings-details.component';
import { NotFoundComponent } from './components/pages/not-found/not-found.component';
import { ResetPasswordComponent } from './components/pages/reset-password/reset-password.component';

const routes: Routes = [
    { path: '', component: HomeDemoOneComponent },
    { path: 'passwordForgettenVerification', component: ComingSoonComponent },
    { path: 'ResetPassword/:email/:code', component: ResetPasswordComponent },
    { path: 'user-profile/:id', component: AuthorProfileComponent },
    { path: 'recherche', component: GridListingsFullWidthComponent },
    { path: 'trajet/:id', component: ListingsDetailsComponent },
    { path: 'dashboard', component: DashboardComponent },
    { path: 'historique', component: DashboardBookingsComponent },
    { path: 'profile', component: DashboardMyProfileComponent },
    { path: 'dashboard-add-listings', component: DashboardAddListingsComponent },
    { path: 'trajets', component: DashboardMyListingsComponent },
    // Here add new pages component

    { path: '**', component: NotFoundComponent } // This line will remain down from the whole pages component list
];

@NgModule({
    imports: [RouterModule.forRoot(routes, { relativeLinkResolution: 'legacy' })],
    exports: [RouterModule]
})
export class AppRoutingModule { }